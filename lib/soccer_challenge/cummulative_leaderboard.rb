# frozen_string_literal: true

module SoccerChallenge
  class CummulativeLeaderboard
    attr_accessor :current_day_number, :team_scores

    def initialize
      @current_day_number = 0
      @team_scores        = Hash.new(0)
    end

    def advance_game_day(game_day_leaderboard)
      self.current_day_number = [self.current_day_number, game_day_leaderboard.day_number].max
      
      game_day_leaderboard.scoreboard.each do |team, points|
        team_scores[team] += points
      end
      
      self
    end

    def team_rankings(limit = nil)
      rows = 
        team_scores.sort_by do |team, total|
          [-total, team.to_s.downcase]
        end
      
      rows = rows.first(limit) unless limit.nil?

      rows
    end
  end
end
