# frozen_string_literal: true

module SoccerChallenge
  class UI
    attr_reader :options

    MAX_CONSOLE_WIDTH = 160

    def initialize(options)
      @options = options
    end

    def output
      options.output_io
    end

    def display_leaderboard(leaderboard)
      if leaderboard.current_day_number > 1
        # add a blank line to separate day output
        say('')
      end

      say('Matchday %i', leaderboard.current_day_number)

      leaderboard.team_rankings(options.rankings_to_show).each do |team, points|
        unit = points == 1 ? 'pt' : 'pts'
        say("%s, %i %s", team, points, unit)
      end

      self
    end
    
    def report_error(error)
      return self unless options.verbose?
      say("[!] %s", error.message)
    end

    def say(text, *args)
      text = text.to_s
      text = args.any? ? format(text, *args) : text
      output.puts(text)
    end
  end
end
