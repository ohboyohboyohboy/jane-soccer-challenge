# frozen_string_literal: true

module SoccerChallenge
  module Errors
    Error = Class.new(StandardError)
    ParsingError = Class.new(Error)
  end
end
