# frozen_string_literal: true

module SoccerChallenge
  class Options
    # just declaring this for a little extra bit of readability
    # calling initialize without any arguments forces the default behavior
    def self.default
      self.new
    end

    attr_accessor :verbose, 
      :input_file_name, 
      :output_file_name, 
      :input_io, 
      :output_io, 
      :rankings_to_show

    alias_method :verbose?, :verbose

    def initialize(
      verbose: false,
      input_file_name: nil,
      output_file_name: nil,
      input_io: nil,
      output_io: nil,
      rankings_to_show: 3
    )
      @verbose = !!verbose
      @input_file_name = input_file_name
      @output_file_name = output_file_name
      @input_io = input_io
      @output_io = output_io
      @rankings_to_show = rankings_to_show
    end

    def input_io
      @input_io ||= input_file_name ? open(input_file_name, 'r') : ARGF
    end

    def output_io
      @output_io ||= output_file_name ? open(output_file_name, 'w') : $stdout
    end
  end
end
