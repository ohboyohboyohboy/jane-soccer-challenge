# frozen_string_literal: true

require 'optparse'

module SoccerChallenge
  class Main
    include Errors
    include GameResultParsing

    def self.run!(argv = ARGV)
      new(argv).tap(&:run!)
    end

    attr_reader :argv, :leaderboard, :current_game_day

    def initialize(argv = ARGV)
      @argv                    = argv
      @current_game_day = nil
      @input                   = nil
      @leaderboard             = nil
      @options                 = nil
      @ui                      = nil
    end

    def input
      @input ||= options.input_io
    end

    def options
      @options ||= Options.default
    end

    def ui
      @ui ||= UI.new(options)
    end

    def run!
      parse_options!
      setup!
      main_loop!
      terminate!

      self
    end

    private

    def parse_options!
      option_parser.parse!(argv)

      self
    end

    def setup!
      @leaderboard      = CummulativeLeaderboard.new
      @current_game_day = GameDayLeaderboard.new(day_number: 1)
    end

    def main_loop!
      until input.eof?
        process_line!(input.gets.chomp)
      end
    end

    def terminate!
      refresh_leaderboard
      @current_game_day = nil
      
      self
    end

    def process_line!(line)
      game_result = parse_game_result(line)


      unless current_game_day.can_add?(game_result)
        advance_game_day!
      end

      current_game_day.add_game_result(game_result)
    rescue ParsingError => err
      ui.report_error(err)
    end

    def advance_game_day!
      refresh_leaderboard

      current_day_number = current_game_day&.day_number || 1
      @current_game_day = GameDayLeaderboard.new(day_number: current_day_number + 1)
    end

    def refresh_leaderboard
      unless current_game_day.nil? || current_game_day.empty?
        leaderboard.advance_game_day(current_game_day)
        ui.display_leaderboard(leaderboard)
      end
    end

    def option_parser
      @option_parser ||=
        OptionParser.new do |o|
          program_name = File.basename($0)

          o.banner = "#{program_name} [options] [FILE_1 ...]"
          o.separator ''
          o.separator '[General Options]'
          o.separator ''

          o.on('-V', '--verbose', 'Display parsing errors and other details while processing') do
            options.verbose = true
          end

          o.on('-v', '--version', 'Print version and exit') do
            ui.say SoccerChallenge::VERSION
            exit
          end

          o.separator ''
          o.separator '[Input/Output Options]'
          o.separator ''

          o.on('-i', '--input=FILE_NAME', 'Read input data exclusively from FILE_NAME and not STDIN') do |file_name|
            options.input_file_name = file_name
          end

          o.on('-o', '--output=FILE_NAME', 'Write game reports to FILE_NAME instead of STDOUT') do |file_name|
            options.output_file_name = file_name
          end

          o.on('-n NUM', '--display-count=NUM', Integer, 'Display top NUM ranking teams when transitioning between days (Default 3)') do |n|
            options.rankings_to_show = n
          end
        end
    end
  end
end
