# frozen_string_literal: true

module SoccerChallenge
  GameResult = Struct.new(:team_a_name, :team_a_score, :team_b_name, :team_b_score) do
    def draw?
      team_a_score == team_b_score
    end

    def team_a_won?
      team_a_score > team_b_score
    end

    def team_b_won?
      team_a_score < team_b_score
    end
  end
end
