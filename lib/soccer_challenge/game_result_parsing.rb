# frozen_string_literal: true

module SoccerChallenge
  module GameResultParsing
    SCORE_DECLARATION_RX = %r<^(\S.*?)\s+(\d+)$>

    module_function

    def parse_game_result(result_spec_line)
      line = result_spec_line.to_s.strip
      segment_count = line.count(',') + 1

      unless segment_count == 2
        result_parsing_error!(line, "Line must contain exactly 2 comma separated score declarations")
      end

      side_a, side_b = line.split(",", 2)
      name_a, score_a = parse_score_declaration(side_a)
      name_b, score_b = parse_score_declaration(side_b)

      GameResult.new(name_a, score_a, name_b, score_b)
    end

    def parse_score_declaration(score_declaration)
      text = score_declaration.to_s.strip

      if text =~ SCORE_DECLARATION_RX
        [$1.strip, $2.to_i]
      else
        result_parsing_error!(score_declaration, "invalid score declaration")
      end
    end

    def result_parsing_error!(bad_text, message)
      fail Errors::ParsingError, "#{bad_text.inspect}: #{message}"
    end
  end
end
