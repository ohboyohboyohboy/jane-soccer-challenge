# frozen_string_literal: true

module SoccerChallenge
  class GameDayLeaderboard
    DRAW_SCORE_VALUE = 1
    LOSE_SCORE_VALUE = 0
    WIN_SCORE_VALUE  = 3
    
    attr_accessor :day_number, :scoreboard

    def initialize(
      day_number: 1,
      game_results: nil
    )
      @day_number   = day_number.to_i
      @scoreboard   = Hash.new(0)
      
      game_results&.any? and game_results.each { |res| add_game_result(res) }
    end

    def empty?
      scoreboard.empty?
    end

    def can_add?(result)
      !team_registered?(result.team_a_name, result.team_b_name)
    end

    def add_game_result(result)
      case
      when result.draw?
        scoreboard[result.team_a_name] += DRAW_SCORE_VALUE
        scoreboard[result.team_b_name] += DRAW_SCORE_VALUE
      when result.team_a_won?
        scoreboard[result.team_a_name] += WIN_SCORE_VALUE
        scoreboard[result.team_b_name] += LOSE_SCORE_VALUE
      when result.team_b_won?
        scoreboard[result.team_a_name] += LOSE_SCORE_VALUE
        scoreboard[result.team_b_name] += WIN_SCORE_VALUE
      end

      self
    end

    def team_registered?(*names)
      names.any? { |name| scoreboard.key?(name) }
    end
  end
end
