# frozen_string_literal: true

require 'soccer_challenge/version'
require 'optparse'

lib_dir = File.expand_path(__dir__)
$LOAD_PATH.unshift(lib_dir) unless $LOAD_PATH.include?(lib_dir)

module SoccerChallenge
  autoload :CummulativeLeaderboard, 'soccer_challenge/cummulative_leaderboard'
  autoload :Errors, 'soccer_challenge/errors'
  autoload :GameDayLeaderboard, 'soccer_challenge/game_day_leaderboard'
  autoload :GameResult, 'soccer_challenge/game_result'
  autoload :GameResultParsing, 'soccer_challenge/game_result_parsing'
  autoload :Main, 'soccer_challenge/main'
  autoload :Options, 'soccer_challenge/options'
  autoload :UI, 'soccer_challenge/ui'
end
