# Design Notes

[Original Problem Specification in PROMPT.md](./PROMPT.md)

The problem specification includes a requirement to create "a design document explaining your thought process." Since it does not specify a required format or degree of depth, I am choosing to share some of my critical thoughts more loosely. I aim to communicate my thought process as if speaking in real time to an interviewer.

## Thoughts After Reviewing Prompt Document

* The project will be a command line script in a git repository, so a typical gem skeleton generated from `bundle gem` will work well to accomplish the task.
* One of the most important points of emphasis in the description is that the **input data could be in the order of terabytes**:
  * We should not take a more quick-and-dirty naive approach where the script reads in all input data in one go, then scans and extracts the game results using regular expressions. This approach requires the full input to be closed and present, where the prompt specifies that this should operate in a continuously input-streaming fashion.
  * Instead, we need to take a highly streaming-oriented IO-centric approach where we process input line by line, outputting the leaderboard summary along the way.
* The requirement of accepting one or more file names as arguments to the script, falling back to STDIN when none are present, makes Ruby's special [`ARGF`](https://ruby-doc.org/core-2.5.0/ARGF.html) object a handy and efficient way to obtain a default input IO handle.

### Game Result Format

* The prompt does not specify a precise format for game result lines. Rather we know a game result is expressed with these characteristics:
  * A single line of text in input represents a single game result.
  * Two comma-separated entries should be in a line, each containing a team name and an integer score.
  * A general format is implied in the `sample-input.txt` file.
* So, rather than impose a rigid grammar with more complex lexing/parsing approaches, we keep the parsing quick but robust:
  * Each step along the way, we'll apply `String#strip` to allow a lot of format flexibility by ignoring space characters when extracting the important data.
  * We will make the assumption that team names will never themselves contain commas, so we do not need to worry about implementing a means to escape a non-separator comma ala CSV parsing.
  * Check the comma count in the line -- it should be exactly 1. If not, it's a badly formatted line that will be ignored.
  * After splitting into a left and right string around the comma, each fragment should end with a zero-or-positive integer value, which we can capture and validate with a simple regular expression. Anything before the matched number is stripped and considered the associated team name.

### GameDay Detection

* One of the main determinations left ambiguous in the problem statement is how we detect a transition between two different game days based solely on the game result line inputs. It states:

> The input contains results of games, one per line and grouped by matchday. All
> teams play exactly once during a matchday, for example given a league of six teams,
> each matchday would consist of three games. There is no specific delimiter
> between matchdays so your application should recognize the start and end of
> a matchday. See sample-input.txt as an example.
> - the only provided input is the result of games, no other type of input is provided to the software
> - the number of teams participating in a league is unknown and should be determined by the software
> - the teams participating in a league are unknown and should be determined by the software
> - the input file is provided for simplicity, this is a simulation for a real-time system

* The key hint is that *"all teams play exactly once during a matchday"*:
  * As we process results line by line, we keep a running set of team names we have seen in the current day
  * When a game result contains a team name we have already seen in the current game day, **this is a signal that a new game day series of results is starting**

### Package Structure

* I used a simple gem skeleton directory for the repository, as generated with `bundle gem soccer_challenge`.
* I also followed the typical pattern of setting up a `lib` directory with a camelized project name as the outer containing module namespace (`SoccerChallenge`).
* I chose RSpec for my testing framework, placing tests in the `spec` directory.

### Command-Line Script Architecture

* I value clean, self-documenting command line executables that use the standard `optparse` library to implement `--help` and `--version` switches at minimum.
* To keep the project clean, organized, modular, and readable, I used some common class patterns for implementing a command line script.

####  `SoccerChallenge::Main`

* This is the main driver of the program; it is where the primary logic of the script is actually written.
* It instantiates the main components -- the `UI` instance and the `Options` instance
* It handles parsing the given ARGV options using an `OptionParser` from `optparse` and applying any customizations to its `Options` object

#### `SoccerChallenge::Options`

* A simple plain-old object that tracks the variable parameters of the app, which will typically be altered when the `Main` instance runs the option parser.
* It is also where we establish default values.
* Keeping the script parameters all together in a single object like this makes extending the tool much cleaner and easier. For example, I added the `--display-count=NUM` option to allow displaying more than just the top 3 teams in a given game day without much additional complication.

#### `SoccerChallenge::UI`

* UI represents the "user interface" of the command line script.
* It is a simple wrapper around the console IO methods to display output data.
* I have also used a similar pattern previously for more interactive scripts where the user may be prompted with questions -- in other words, it focuses on talking directly to the user.
* For this script, we only needed to implement two simple output reporting methods
  * `display_leaderboard(leaderboard)` -- prints the current game day and the top three cumulative team scores
  * `report_error(error)` -- optionally displays game data parsing errors if `--verbose` is specified


### Writing Tests

* I decided to implement two types of tests for this project:
  * `{CLASS_NAME}_spec.rb` files -- unit tests covering the modeling objects and their public interface methods (e.g. `GameResult`, `GameDayLeaderboard`, etc...)
  * `cli_spec.rb` -- a very simple functional end-to-end test that executes the full command line script with some specified input, captures the output to validate it is as expected
  * I tried to cover the happy path cases in each batch of specs at a minimum, adding additional examples to check further options and edge-cases.
