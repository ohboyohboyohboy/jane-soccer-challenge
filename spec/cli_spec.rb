require 'open3'
require 'shellwords'

EXE_PATH = File.expand_path(File.join(__dir__, '../exe/soccer-challenge'))

RSpec.describe "exe/soccer-challenge" do
  include Open3

  let(:well_formed_input) do
    <<~DATA
      San Jose Earthquakes 3, Santa Cruz Slugs 3
      Capitola Seahorses 1, Aptos FC 0
      Felton Lumberjacks 2, Monterey United 0
      Felton Lumberjacks 1, Aptos FC 2
      Santa Cruz Slugs 0, Capitola Seahorses 0
      Monterey United 4, San Jose Earthquakes 2
      Santa Cruz Slugs 2, Aptos FC 3
      San Jose Earthquakes 1, Felton Lumberjacks 4
      Monterey United 1, Capitola Seahorses 0
      Aptos FC 2, Monterey United 0
      Capitola Seahorses 5, San Jose Earthquakes 5
      Santa Cruz Slugs 1, Felton Lumberjacks 1
    DATA
  end

  it "displays help with --help" do
    out = run_app!('--help')
    expect(out).to match("[General Options]")
    expect(out).to match("-V, --verbose")
    expect(out).to match("-v, --version")
  end

  it "displays the version number with --version" do
    out = run_app!('--version')
    expect(out).to match(SoccerChallenge::VERSION)
  end

  it "processes sample well-formed input and reports the league scores" do
    expected_output = <<~DATA
      Matchday 1
      Capitola Seahorses, 3 pts
      Felton Lumberjacks, 3 pts
      San Jose Earthquakes, 1 pt
      
      Matchday 2
      Capitola Seahorses, 4 pts
      Aptos FC, 3 pts
      Felton Lumberjacks, 3 pts
      
      Matchday 3
      Aptos FC, 6 pts
      Felton Lumberjacks, 6 pts
      Monterey United, 6 pts
      
      Matchday 4
      Aptos FC, 9 pts
      Felton Lumberjacks, 7 pts
      Monterey United, 6 pts
    DATA

    out = run_app!(input: well_formed_input)
    expect(out).to eq(expected_output)
  end

  def run_app!(*args, input: nil)
    command = build_command(*args)
    warn(command) if $VERBOSE
    
    output =
      popen3(command) do |stdin, stdout, stderr|
        unless input.nil?
          stdin.write(input)
          stdin.flush
        end

        yield(stdin, stdout, stderr) if block_given?

        stdin.close unless stdin.closed?

        error_text = stderr.read.to_s.strip
        $stderr.puts(error_text) unless error_text.empty?

        stdout.read
      end
  end

  def build_command(*args)
    [EXE_PATH, *args].shelljoin
  end
end
