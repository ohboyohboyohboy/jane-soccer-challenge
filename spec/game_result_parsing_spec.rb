RSpec.describe SoccerChallenge::GameResultParsing do
  subject { SoccerChallenge::GameResultParsing }

  it "successfully parses a valid game result spec line" do
    game_result = subject.parse_game_result("Monterey United 1, Capitola Seahorses 0")
    expect(game_result).to be_an_instance_of(SoccerChallenge::GameResult)
    expect(game_result.team_a_name).to eq("Monterey United")
    expect(game_result.team_a_score).to eq(1)
    expect(game_result.team_b_name).to eq("Capitola Seahorses")
    expect(game_result.team_b_score).to eq(0)
  end

  it "raises an error when result spec line has too few comma-separated sections" do
    expect {
      subject.parse_game_result("Monterey United 1")
    }.to raise_error SoccerChallenge::Errors::ParsingError
  end

  it "raises an error when result spec line has too many comma-separated sections" do
    expect {
      subject.parse_game_result("Monterey United 1, Tampa Thingos 10, Yosemite Yodlers 12")
    }.to raise_error SoccerChallenge::Errors::ParsingError
  end

  it "raises an error when a score declaration is not a name followed by an integer" do
    expect {
      subject.parse_game_result("Monterey United, Tampa Thingos 10")
    }.to raise_error SoccerChallenge::Errors::ParsingError
  end
end
