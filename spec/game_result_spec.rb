RSpec.describe SoccerChallenge::GameResult do
  let(:team_a_name) { "Monterey United" }
  let(:team_a_score) { 1 }
  let(:team_b_name) { "Capitola Seahorses" }
  let(:team_b_score) { 1 }

  subject do
    described_class.new(
      team_a_name,
      team_a_score,
      team_b_name,
      team_b_score
    )
  end

  context "When the game was a draw" do
    let(:team_a_score) { 1 }
    let(:team_b_score) { 1 }

    it "should report that it is a draw" do
      expect(subject.draw?).to be_truthy
    end

    it "should not report that team a won" do
      expect(subject.team_a_won?).to be_falsey
    end

    it "should not report that team b won" do
      expect(subject.team_b_won?).to be_falsey
    end
  end

  context "When team a scored higher" do
    let(:team_a_score) { 2 }
    let(:team_b_score) { 1 }

    it "should not report that it is a draw" do
      expect(subject.draw?).to be_falsey
    end

    it "should report that team a won" do
      expect(subject.team_a_won?).to be_truthy
    end

    it "should not report that team b won" do
      expect(subject.team_b_won?).to be_falsey
    end
  end
end
