RSpec.describe SoccerChallenge::GameDayLeaderboard do
  subject { described_class.new }

  let(:game_1) { game('A', 3, 'B', 5) }
  let(:game_2) { game('C', 2, 'D', 2) }
  let(:game_3) { game('E', 0, 'F', 10) }
  let(:game_4) { game('A', 2, 'D', 2) }
  let(:game_5) { game('E', 5, 'C', 5) }

  def game(name_a, score_a, name_b, score_b)
    SoccerChallenge::GameResult.new(name_a, score_a, name_b, score_b)
  end

  context 'When no game results have been added' do
    it 'has an empty scoreboard' do
      expect(subject).to be_empty
    end

    it 'can add any game record' do
      expect(subject.can_add?(game_1)).to be_truthy
      expect(subject.can_add?(game_4)).to be_truthy
    end

    it 'has no registered teams' do
      expect(subject.team_registered?('A')).to be_falsey
      expect(subject.team_registered?('B', 'C')).to be_falsey
    end
  end

  context 'When one game result has been added' do
    before do
      subject.add_game_result(game_1)
    end

    it 'does not have an empty scoreboard' do
      expect(subject).to_not be_empty
    end

    it 'can add any game record that includes only unregistered teams' do
      expect(subject.can_add?(game_1)).to be_falsey
      expect(subject.can_add?(game_2)).to be_truthy
      expect(subject.can_add?(game_4)).to be_falsey
    end

    it 'registers the first two team names' do
      expect(subject.team_registered?('A')).to be_truthy
      expect(subject.team_registered?('B')).to be_truthy
      expect(subject.team_registered?('C', 'D')).to be_falsey
    end
  end

  context 'When all games in the day have been added' do
    before do
      subject.add_game_result(game_1)
      subject.add_game_result(game_2)
      subject.add_game_result(game_3)
    end

    it 'does not have an empty scoreboard' do
      expect(subject).to_not be_empty
    end

    it 'can add any game record that includes only unregistered teams' do
      expect(subject.can_add?(game_1)).to be_falsey
      expect(subject.can_add?(game_2)).to be_falsey
      expect(subject.can_add?(game_4)).to be_falsey
    end

    it 'registered all team names' do
      %w( A B C D E F ).each do |team|
        expect(subject.team_registered?(team)).to be_truthy
      end
    end
  end
end
