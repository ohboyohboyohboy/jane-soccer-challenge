RSpec.describe SoccerChallenge::CummulativeLeaderboard do
  subject { described_class.new }

  let(:game_day_1) do
    mock_game_day(
      day_number: 1,
      scoreboard: {
        'A' => 3,  'B' => 1,
        'C' => 0,  'D' => 1
      }
    )
  end

  let(:game_day_2) do
    mock_game_day(
      day_number: 2,
      scoreboard: {
        'A' => 1,  'B' => 1,
        'C' => 3,  'D' => 1,
        'E' => 1,  'F' => 0,
      }
    )
  end

  let(:game_day_3) do
    mock_game_day(
      day_number: 3,
      scoreboard: {
        'A' => 0,  'B' => 3,
        'C' => 0,  'D' => 3,
        'E' => 0,  'F' => 3,
      }
    )
  end

  def mock_game_day(day_number: 1, scoreboard: {})
    double(
      'GameDayLeaderboard',
      day_number: day_number,
      scoreboard: scoreboard
    )
  end

  context "When no days have been added" do
    it 'returns an empty list of team_rankings' do
      expect(subject.team_rankings).to be_empty
    end
  end

  context "After adding one game day" do
    before do
      subject.advance_game_day(game_day_1)
    end

    it 'returns the cummulative team score sorted by highest score and then team name' do
      rankings = subject.team_rankings
      expect(rankings).to eq([
        ['A', 3],
        ['B', 1],
        ['D', 1],
        ['C', 0]
      ])
    end

    it 'returns the first N cummulative team scores sorted when called with argument N' do
      rankings = subject.team_rankings(2)
      expect(rankings).to eq([
        ['A', 3],
        ['B', 1]
      ])      
    end

    it 'reports the current day number correctly' do
      expect(subject.current_day_number).to eq(1)
    end
  end

  context "After adding many game days" do
    before do
      subject.advance_game_day(game_day_1)
      subject.advance_game_day(game_day_2)
      subject.advance_game_day(game_day_3)
    end

    it 'returns the cummulative team score sorted by highest score and then team name' do
      rankings = subject.team_rankings
      expect(rankings).to eq([
        ['B', 5],
        ['D', 5],
        ['A', 4],
        ['C', 3],
        ['F', 3],
        ['E', 1]
      ])
    end

    it 'returns the first N cummulative team scores sorted when called with argument N' do
      rankings = subject.team_rankings(3)
      expect(rankings).to eq([
        ['B', 5],
        ['D', 5],
        ['A', 4]
      ])
    end

    it 'reports the current day number correctly' do
      expect(subject.current_day_number).to eq(3)
    end
  end
end
