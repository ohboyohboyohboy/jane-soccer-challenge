# Hello, Jane!

I am [Kyle Yetter](https://www.linkedin.com/in/kyle-yetter), applying for an engineering position with Jane. This repository contains my implementation of the take-home coding challenge provided to me as part of the interview process.

## Environment Prep

* This program should function correctly with Ruby 2.7 and 3.x
* I use `rvm` to handle ruby version management on my system, and to ensure a normalized environment, the tester may want to use `rvm` or `rbenv` before performing any of the necessary tasks described below

```
cd ~/this/project/root
rvm use  # should use version in .ruby-version, which you may need to install
bundle install
```

## Running the Program

* Follow the instructions described in "Environment Prep" above
* Run the program executable with `bundle`

```
bundle exec ./exe/soccer-challenge --help
# soccer-challenge [options] [FILE_1 ...]
# 
# [General Options]
# 
#     -V, --verbose                    Display parsing errors and other details while processing
#     -v, --version                    Print version and exit
# 
# [Input/Output Options]
# 
#     -i, --input=FILE_NAME            Read input data exclusively from FILE_NAME and not STDIN
#     -o, --output=FILE_NAME           Write game reports to FILE_NAME instead of STDOUT
#     -n, --display-count=NUM          Display top NUM ranking teams when transitioning between days (Default 3)
```

* To enter game result lines directly into standard input:

```
bundle exec ./exe/soccer-challenge  # no arguments needed
```

* To read input from the file "sample-input.txt":

```
bundle exec ./exe/soccer-challenge sample-input.txt
# OR
bundle exec ./exe/soccer-challenge < sample-input.txt
```

## Running Specs

* Follow the instructions described in "Environment Prep" above
* To run the full suite of specs in the `specs` directory:

```
bundle exec rake spec
```

## Sample Input Files

* This repo includes two sample input files to assist with testing:
  * [sample-input.txt](./sample-input.txt) is Jane's well-formed sample input file provided with the prompt
  * [huge-input.txt](./huge-input.txt) is a well-formed sample input document I generated with a huge amount of data -- it has 1,000,000 lines and is 24MB of data. Pipe it into the script to see how well it holds up processing a huge amount of data

## Design Guide

* Please check out [DESIGN.md](./DESIGN.md) for notes on my design process
* The original prompt document can be found in [PROMPT.md](./PROMPT.md)
